/**
 * config.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

const env = process.env.NODE_ENV;
const os = require('os');
const colors = require('colors');
const originsWhitelist = [ 'http://localhost:3000'];
const responses = {
	status_codes: {
	  'CREATED': 200,
	  'FETCHED': 200,
	  'DELETED': 200,
	  'UPDATED': 200,
	  'NOT_CREATED': 102,
	  'NOT_FETCHED': 202,
	  'NOT_DELETED': 302,
	  'NOT_UPDATED': 402,
	  'BAD_REQUEST': 500,
	  'SERVER_ERROR': 600,
	  'FORBIDDEN': 700,
	  'UNAUTHORIZED': 401,
	  'CONFLICT': 409,
	  'NOT FOUND': 404
	},
	messages: {
	  'created': 'Item(s) creation successful',
	  'notCreated': 'Item(s) creation unsuccessful',
	  'fetch': 'Item(s) retrieval successful',
	  'notFetched': 'Item(s) retrieval unsuccessful',
	  'updated': 'Items(s) update successful',
	  'notUpdated': 'Item(s) update unsuccessful',
	  'deleted': 'Item(s) deletion successful',
	  'notDeleted': 'Item(s) deletion unsuccessful',
	  'badRequest': 'Invalid request',
	  'serverError': 'Requets could be completed due to server issues',
	  'forbidden': 'You do not have the permission to perform this action',
	  'unAuthorized': 'You are not authorized to perform this action'
	}
  };


let conf = {
	port: 9003,
	hostname: os.hostname(),
	api_title: 'IUM_LIBRARY_SYS',
	db_url: `mongodb://localhost:27017/IUM_LIB?authMechanism=SCRAM-SHA-1&authSource=admin`,
	auth: {
	  secret: process.env.AUTH_SECRET,
	  name: 'IUM_LIB',
	  saveUninitialized: false,
	  resave: true,
	  maxAge: '1d'
  
	},
	response: responses,
	originsWhitelist: originsWhitelist,
	SMTP_CONFIGS: {
	  service: 'smtp.gmail.com',
	  pool: true,
	  secure: false,
	  port: 25,
	  proxy: '',
	  host: 'mail.gmail.com',
	  auth: {
		user: 'contact@erassy.com',
		pass: '0817246654'
	  },
	  logger: true, // log to console
	  debug: true,
	  tls: {
		ciphers: 'SSLv3',
		rejectUnauthorized: false
	  }
	},
  
	encryptionConfigs: {
	  algorithm: 'aes-256-cbc',
	  pass: process.env.ENCRYPTION_PASS || 'P@55w0rd@1UM_l18_P@55',
	  key: process.env.ENCRYPTION_KEY || 'P@55w0rd@S1UM_l18_K3y'
	},
  
	JWT: {
	  secret: '@borrowthebookyouneed@',
	  options: {
		expiresIn: '24h',
		issuer: 'IUM Library System'
	  }
	}
  };
module.exports = conf;