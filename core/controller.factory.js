/**
 *
 * @author Erastus Nathingo <erastus.nathingo@standardbank.com.na>
 * @description Library for creating root controllers
 * @copyright (c) 2017 Standard Bank Namibia
 */


const Controller = require('./base.controller');
const Log = require('../lib/logger.lib');
let logger = new Log();
const _ = require('lodash');

let ControllerFactory = function () {
    return {
        create: function (model) {
            let Model = require("../models/" + model);
            return new Controller(Model);
        }
    }
}
module.exports = ControllerFactory();