let models = function(Controller){

  this.mtc_p0 =  () => this.createController('sms_priority_models/mtc/mtc_p0.model');
  this.mtc_p1 =  () => this.createController('sms_priority_models/mtc/mtc_p1.model');
  this.mtc_p2 =  () => this.createController('sms_priority_models/mtc/mtc_p2.model');
  this.mtc_p3 =  () => this.createController('sms_priority_models/mtc/mtc_p3.model');

  this.tn_p0 = () => this.createController('sms_priority_models/tn/tn_p0.model');
  this.tn_p1 = () => this.createController('sms_priority_models/tn/tn_p1.model');
  this.tn_p2 = () => this.createController('sms_priority_models/tn/tn_p2.model');
  this.tn_p3 = () => this.createController('sms_priority_models/tn/tn_p3.model');


  this.mtc_enroute_primary = () =>this.createController('sms_priority_models/mtc/mtc_enroute.model');
  this.tn_enroute_primary = () =>this.createController('sms_priority_models/tn/tn_enroute.model');

  this.sms = () =>this.createController('sms.model');
  this.otp = () =>this.createController('otp.model');
  
  this.campaign = () => this.createController('campaign.model');

  this.inbox = ()=> this.createController('inbox.model');
  


  this.roles = () =>this.createController('user.roles.model');
  this.costCenter = () =>this.createController('costCenter.model');

  /** MTC portability controller */
  this.mtc_port_list = ()=> this.createController('../models/sms_priority_models/mtc/mtc_port_list.model');
  /** TN Mobile portability controller */
  this.tn_port_list = ()=> this.createController('../models/sms_priority_models/tn/tn_port_list.model');


  /**
   * @description Creates controller for Scheduled Messages model
   * @name Scheduled_Messages_Controller
   * @returns Scheduled Messages controller
   */
  this.scheduled = () =>this.createController('scheduled.model');
  /**
   * @description Creates controller for MNO model
   * @name MNO_Controller
   * @returns MNO controller
   */
  this.mno = () =>this.createController('mno.model');

  this.createController = (model) =>Controller.create(model);
};


module.exports = models;
