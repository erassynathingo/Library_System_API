var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var config = require('./config'); // get our config file
var User = require('./models/user'); // get our mongoose model
var moment = require('moment');
const http = require('http');
const attach = require('./lib/attach.lib');
const headers = require('./middleware/headers');

const app = express();
const server = http.createServer(app).listen(config.port, () => {
	console.log(`IUM Lib System on ${config.hostname}:${config.port}`);
})

const users = require('./routes/users.route')
const auth = require('./routes/auth.route')
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({
		extended: false
	}))
	.use(bodyParser.json())
	.use(morgan('dev'))
	.use(headers)
	.use(bodyParser.urlencoded({ extended: true, limit: '1000mb' }))
	.use(bodyParser.json({ limit: '100mb' }))
	.use(bodyParser.raw({ limit: '100mb' }))
	.use(attach)


	// ROUTES
	.use('/users', users)
	.use('/auth', auth)




	.use('/', (req, res) => {
		res.status(200).json({
			api: config.api_title,
			copyright: `@2018`,
			version: '1.0.0'
		})
	})