/**
* @author Erastus Nathingo <erastus.nathingo@standardbank.com.na>
* @author Leonard Shivute  <Leonard.Shivute@standardbank.com.na> 
* @module OTP_Model
* @description Provides a mongoose model for OTP's, Alerts and single message
* @param
* @returns User
* @throws 
*/
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

/** OTP schema definitions */

let otpSchema =
{
    id: { type: String},
  author: {
      firstName: { type: Object, required: true},
      lastName: { type: Object, required: true}
  },
  type: { type: String, required: true},
  publisher: { type: String, required: true },
  type: { type: String, required: true },
  tags: { type: Array, required: true, default: ['general']},
  available : { type: boolean, default: true },
  lastBorrowed:  {
      date : { type: Date, default: new Date()},
      borrowerID: { type: String, default: ''}
  },
  overdueCost: {type: Number, default: 0.0},
  entry_date: { type: Date, default: new Date()}
};


let dictionary = {
    id: '_id',
    author: 'author',
    type: 'type',
    publisher: 'publisher',
    type: 'type',
    tags: 'tags',
    available : 'available',
    lastBorrowed:  'lastBorrowed',
    overdueCost: 'overdueCost',
    entry_date: 'entry_date'
  }

const createExclude = ['entry_date'];
const updateExclude = ['_id', 'entry_date','author'];
const readExclude = [];
const model = Model.create('Books', otpSchema, dictionary, updateExclude);
module.exports = model.getModel()
