/**
 * users.model.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

 
let _config = require('../config');
const Model = require('../core/model');
const moment = require('moment');

const MAX_LOGIN_ATTEMPTS = 5;
const LOCK_TIME = 2;

/** User schema definitions */
let userSchema =
{
  _id: { type: String },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  cellphone: { type: String, required: true },
  borrowingHistory: { type: Array, default: [{bookId: '', borrowedDate: '', expectedReturn: '', overdueCost: 0.0}] },
  outStanding: { type: Array, default: [] },

  role: { type: String, required: true },
  loginAttempts: {type: Number, default: 0 },
  active: { type: Boolean, required: true, default: false },
  lockUntil: { type: Date},
  approved: { type: Boolean, required: false, default: false },
  approvedBy: { type: Object, default: {id: ''} },
  entry_date: { type: Date, default: moment().toDate() }
};

let dictionary = {
  _id: '_id',
  firstName: 'firstName',
  lastName: 'lastName',
  loginAttempts: 'loginAttempts',
  active: 'active',
  username: 'username',
  cellphone: 'cellphone',
  approved: 'approved',
  approvedBy: 'approvedBy',
  email: 'email',
  lockUntil: 'lockUntil',
  password: 'password',
  role: 'role',
  entry_date: 'entry_date'
};

const createExclude = ['entry_date'];
const updateExclude = ['id_number', 'entry_date', 'username'];
const readExclude = [];
const model = Model.create('Users', userSchema, dictionary, createExclude, updateExclude);

model.model.methods.incLoginAttempts = function () {
  // if we have a previous lock that has expired, restart at 1
  if (this.lockUntil && this.lockUntil < moment().toDate()) {
    return this.update({
      $set: { loginAttempts: 1 },
      $unset: { lockUntil: 1 }
    });
  }
  // otherwise we're incrementing
  var updates = { $inc: { loginAttempts: 1 } };
  // lock the account if we've reached max attempts and it's not locked already
  if (this.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
    updates.$set = { lockUntil: moment().add(LOCK_TIME,'hours') };
  }
  return this.update(updates);
};

module.exports = model.getModel();
