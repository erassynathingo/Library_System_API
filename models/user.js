var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('User', new Schema({ 
	firstname: {type:String,
				required:[true,"Please enter First Name"]
	             },
	email: {type:String,
		required:[true,"Please enter your email address"],
		unique:true
		 },
	identityNo:{type:Number,
		required:[true,"Please enter your Identity Number"],
		unique: true 
		 },
	password: {type:String,
		required:[true,"Please enter the password"]
		 },
	role:{type:String,
		required:[true,"Please enter your role"]
		 },
	course:{type:String,
		required:[true,"Please enter the Course Name"]
		 },
	institution:{type:String,
		required:[true,"Please enter the Institution"]
		 },
    borrowDate:{type:Date,
			required:[true,"Please enter the Borrowing Date"]
			 }, 
	returnDate:{type:Date,
		required:[true,"Please enter the return Date"]
		 }, 
	borrowingHistory:String,
	outStanding:String
}));