/**
 * error-handler.js
 *
 * @author Erastus Nathingo <erastus.nathingo@standardbank.com.na>
 * @copyright (c) 2017 Standard Bank Namibia
 * All rights reserved
 */
const errorHandler = require('../lib/errorHandler.lib');
module.exports = errorHandler;