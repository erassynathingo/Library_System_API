/**
 * Import and export of responder
 *
 * @author Erastus Nathingo <erastus.nathingo@standardbank.com.na>
 * @copyright (c) 2017 Standard Bank Namibia
 * All rights reserved
 */
const responder = require('../lib/api.responder');

module.exports = responder;