function CampainError(message) {
    this.name = 'CampainError';
    this.message = message || 'Campaign Error';
    this.stack = (new Error()).stack;
}
CampainError.prototype = Object.create(Error.prototype);
CampainError.prototype.constructor = CampainError;

module.exports = CampainError;